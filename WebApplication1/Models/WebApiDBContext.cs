﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Models
{
    public class WebApiDBContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }

        public WebApiDBContext(){}
        public WebApiDBContext(DbContextOptions<WebApiDBContext> ops)
            : base (ops)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>()
                .HasOne(p => p.Customer)
                .WithMany(b => b.Orders)
                .HasForeignKey(p => p.CustomerId)
                .HasConstraintName("ForeignKey_Order_Customer");
        }

        public override int SaveChanges()
        {
            CheckCreatedAt();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            CheckCreatedAt();
            return await base.SaveChangesAsync();
        }

        private void CheckCreatedAt()
        {
            var entityEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is Order && (x.State == EntityState.Added || x.State == EntityState.Modified));


            foreach (var ee in entityEntries)
            {
                var order = ee.Entity as Order;
                if (ee.State == EntityState.Added)
                    order.CreatedDate = DateTime.UtcNow;
            }
        }
    }
}
