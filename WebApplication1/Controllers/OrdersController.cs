﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Produces("application/json")]
    [Route("api/Orders")]
    public class OrdersController : Controller
    {
        private readonly WebApiDBContext _context;
        private readonly ILogger _logger;
        public OrdersController(WebApiDBContext context, ILogger<CustomersController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Orders/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrder([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var order = await _context.Orders.SingleOrDefaultAsync(m => m.Id == id);          
                if (order == null) return NotFound("Order not found");
                return Ok(order);
            }
            catch(Exception e)
            {
                _logger.LogError(e, $"Error getting order {id}");
                return StatusCode(500);
            }
        }
        // POST: api/Orders
        [HttpPost]
        public async Task<IActionResult> PostOrder([FromBody] Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var found =  await _context.Customers.AnyAsync(c => c.Id == order.CustomerId);
                if(!found) return NotFound("Customer not found");
                _context.Orders.Add(order);
                await _context.SaveChangesAsync();
            }
            catch(Exception e)
            {
                _logger.LogError(e, $"Error posting order {order.Id}");
                return StatusCode(500);
            }

            return CreatedAtAction("GetOrder", new { id = order.Id }, order);
        }


        private bool OrderExists(int id)
        {
            return _context.Orders.Any(e => e.Id == id);
        }
    }
}