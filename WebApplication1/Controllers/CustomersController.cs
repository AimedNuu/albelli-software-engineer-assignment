﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Produces("application/json")]
    [Route("api/Customers")]
    public class CustomersController : Controller
    {
        private readonly WebApiDBContext _context;
        private readonly ILogger _logger;

        public CustomersController(WebApiDBContext context, ILogger<CustomersController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Customers
        [HttpGet]
        [Route("without_orders")]
        public async Task<IActionResult> GetCustomers()
        {
            try
            {
                var customers = await _context.Customers.Where(c => c.Orders == null || c.Orders.Count == 0).ToListAsync();
                if(customers.Count == 0) return NoContent();
                return Ok(customers);
            }
            catch(Exception e)
            {
                _logger.LogError(e, "Error getting customers without orders");
                return StatusCode(500);
            }
        }

        // GET: api/Customers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomer([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var customer = await _context.Customers.SingleOrDefaultAsync(m => m.Id == id);          
                if (customer == null) return NotFound("Customer not found");

                customer.Orders = await _context.Orders.Where(o => o.CustomerId == customer.Id).ToListAsync();

                return Ok(customer);
            }
            catch(Exception e)
            {
                _logger.LogError(e, $"Error getting customer {id}");
                return StatusCode(500);
            }

        }

        // POST: api/Customers
        [HttpPost]
        public async Task<IActionResult> PostCustomer([FromBody] Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _context.Customers.Add(customer);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetCustomer", new { id = customer.Id }, customer);
            }
            catch(Exception e)
            {
                _logger.LogError(e, $"Error posting customer {customer.Id}");
                return StatusCode(500);
            }

        }

        private bool CustomerExists(int id)
        {
            return _context.Customers.Any(e => e.Id == id);
        }
    }
}