﻿using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using WebApplication1.Models;
using Xunit;
using WebApplication1.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Tests
{
    public class OrderControllerTests : TesBase
    {
        [Fact]
        public async void test_post_order_w_customer()
        {
            var orderController = new OrdersController(GetContextWithData(), null);
            var newOrder = new Order
            {
                //db will generate a proper id by seq but in test we have to do in manually
                Id = 2,
                CustomerId = 1,
                Price = 1.0m
            };
            var result = await orderController.PostOrder(newOrder);

            var viewResult = Assert.IsType<CreatedAtActionResult>(result);
            var order = viewResult.Value as Order;
            Assert.True(order != null && order.Price == 1.0m && order.CustomerId == 1);
        }

                [Fact]
        public async void test_post_order_no_customer()
        {
            var orderController = new OrdersController(GetContextNoData(), null);
            var newOrder = new Order
            {
                //db will generate a proper id by seq but in test we have to do in manually
                Id = 2,
                CustomerId = 1,
                Price = 1.0m
            };
            var result = await orderController.PostOrder(newOrder);

            Assert.IsType<NotFoundObjectResult>(result);
        }
    }
}
