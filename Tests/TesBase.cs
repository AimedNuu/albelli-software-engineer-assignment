﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication1.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using WebApplication1.Models;
using Xunit;
using Microsoft.EntityFrameworkCore;

namespace Tests
{
    public class TesBase
    {
        protected WebApiDBContext GetContextWithData()
        {
            var context = GetContextNoData();

            var cust1 = new Customer { Id = 1, Name = "foo", Email = "foo@email.com" };
            var order1 = new Order { Id = 1, CustomerId = 1, Customer = cust1, Price = 5.0m, CreatedDate = System.DateTime.Now };

            var cust2 = new Customer { Id = 2, Name = "bar", Email = "bar@email.com" };

            context.Customers.Add(cust1);
            context.Customers.Add(cust2);

            context.Orders.Add(order1);

            context.SaveChanges();

            return context;
        }
        protected WebApiDBContext GetContextNoData()
        {
            var options = new DbContextOptionsBuilder<WebApiDBContext>()
                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                              .Options;
            var context = new WebApiDBContext(options);

            return context;
        }
    }
}
