using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using WebApplication1.Models;
using Xunit;
using WebApplication1.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Tests
{
    public class CustomerControllerTests : TesBase
    {
        //TODO: CustomersController.GetCustomers() Route test with reflection.
        //https://www.strathweb.com/2012/08/testing-routes-in-asp-net-web-api/


        [Fact]
        public async void test_getcustomers_without_orders_available()
        {
            var controller = new CustomersController(GetContextWithData(), null);

            var result = await controller.GetCustomers();

            var viewResult = Assert.IsType<OkObjectResult>(result);
            var customers = viewResult.Value as List<Customer>;
            Assert.True(customers != null);
            var customer = Assert.IsType<Customer>(customers[0]);
            var orders = customer.Orders as List<Order>;
            Assert.True(orders == null);
            Assert.True(customer.Name == "bar");
        }

        [Fact]
        public async void test_getcustomers_w_content()
        {
            var controller = new CustomersController(GetContextWithData(), null);

            var result = await controller.GetCustomers();
            var viewResult = Assert.IsType<OkObjectResult>(result); 

            var customers = viewResult.Value as List<Customer>;
            Assert.True(customers[0].Id == 2);
            var orders = customers[0].Orders as List<Order>;
            Assert.True(orders == null);
        }
        [Fact]
        public async void test_getcustomers_no_content()
        {
            var controller = new CustomersController(GetContextNoData(), null);

            var result = await controller.GetCustomers();
            var viewResult = Assert.IsType<NoContentResult>(result); 
        }

        [Fact]
        public async void test_getcustomer_not_found()
        {
            var controller = new CustomersController(GetContextNoData(), null);

            var result = await controller.GetCustomer(1);

            var viewResult = Assert.IsType<NotFoundObjectResult>(result);
        }

        [Fact]
        public async void test_getcustomer_with_orders_available()
        {
            var controller = new CustomersController(GetContextWithData(), null);

            var result = await controller.GetCustomer(1);

            var viewResult = Assert.IsType<OkObjectResult>(result);
            var customer = viewResult.Value as Customer;
            Assert.True(customer != null);
            var orders = customer.Orders as List<Order>;
            Assert.True(orders.Count > 0 && orders[0].Price == 5.0m);
        }

        [Fact]
        public async void test_post_customer()
        {
            var controller = new CustomersController(GetContextNoData(), null);
            var newCustomer = new Customer { Id = 1, Email = "bar@mail.com", Name = "foo" };
            var result = await controller.PostCustomer(newCustomer);

            var viewResult = Assert.IsType<CreatedAtActionResult>(result);
            var customer = viewResult.Value as Customer;
            Assert.True(customer != null && customer.Id == newCustomer.Id);
        }

    }
}
